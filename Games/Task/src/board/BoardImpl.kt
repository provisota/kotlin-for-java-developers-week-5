package board

fun createSquareBoard(width: Int): SquareBoard = BoardImpl<Any?>(width)
fun <T> createGameBoard(width: Int): GameBoard<T> = BoardImpl(width)

class BoardImpl<T>(override val width: Int) : GameBoard<T> {
    private val array = Array(width) { Array(width) { Cell(0, 0) } }
    private val map = mutableMapOf<Cell, T?>()

    init {
        for (i in 0 until width) {
            for (j in 0 until width) {
                val newCell = Cell(i + 1, j + 1)
                array[i][j] = newCell
                map[newCell] = null
            }
        }
    }

    override fun get(cell: Cell): T? = map[cell]

    override fun set(cell: Cell, value: T?) {
        map[cell] = value
    }

    override fun filter(predicate: (T?) -> Boolean): Collection<Cell> {
        return map.filterValues { predicate(it) }.keys
    }

    override fun find(predicate: (T?) -> Boolean): Cell? {
        return map.toList().find { predicate(it.second) }?.first
    }

    override fun any(predicate: (T?) -> Boolean): Boolean {
        return map.any { predicate(it.value) }
    }

    override fun all(predicate: (T?) -> Boolean): Boolean {
        return map.all { predicate(it.value) }
    }

    override fun getCellOrNull(i: Int, j: Int): Cell? {
        return if (i !in 1..width || j !in 1..width) null else array[i - 1][j - 1]
    }

    override fun getCell(i: Int, j: Int): Cell {
        checkInRange(i)
        checkInRange(j)
        return array[i - 1][j - 1]
    }

    override fun getAllCells(): Collection<Cell> = array.flatten()

    override fun getRow(i: Int, jRange: IntProgression): List<Cell> {
        checkInRange(i)
        val resultList = ArrayList<Cell>()
        for (j in jRange) {
            if (j !in 1..width) {
                return resultList
            }
            resultList.add(array[i - 1][j - 1])
        }
        return resultList
    }

    override fun getColumn(iRange: IntProgression, j: Int): List<Cell> {
        checkInRange(j)
        val resultList = ArrayList<Cell>()
        for (i in iRange) {
            if (i !in 1..width) {
                return resultList
            }
            resultList.add(array[i - 1][j - 1])
        }
        return resultList
    }

    private fun checkInRange(i: Int) {
        require(i in 1..width)
    }

    override fun Cell.getNeighbour(direction: Direction): Cell? {
        return when (direction) {
            Direction.UP -> getCellOrNull(i - 1, j)
            Direction.DOWN -> getCellOrNull(i + 1, j)
            Direction.RIGHT -> getCellOrNull(i, j + 1)
            Direction.LEFT -> getCellOrNull(i, j - 1)
        }
    }

}