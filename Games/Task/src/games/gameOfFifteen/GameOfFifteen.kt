package games.gameOfFifteen

import board.Cell
import board.Direction
import board.GameBoard
import board.createGameBoard
import games.game.Game

/*
 * Implement the Game of Fifteen (https://en.wikipedia.org/wiki/15_puzzle).
 * When you finish, you can play the game by executing 'PlayGameOfFifteen'.
 */
fun newGameOfFifteen(initializer: GameOfFifteenInitializer = RandomGameInitializer()): Game =
        GameOfFifteen(initializer)

class GameOfFifteen(private val initializer: GameOfFifteenInitializer) : Game {
    private val board = createGameBoard<Int?>(4)
    private val initialList = initializer.initialPermutation

    companion object {
        val SOLUTION = ((1..15).toMutableList() as MutableList<Int?>).apply { add(null) }
    }

    override fun initialize() {
        with(board) {
            var listIndex = 0
            for (i in 0 until width) {
                for (j in 0 until width) {
                    setCellValue(i + 1, j + 1, initialList.getOrNull(listIndex))
                    listIndex++
                }
            }
        }
    }

    override fun canMove(): Boolean = true

    override fun hasWon(): Boolean {
        val boardValues = mutableListOf<Int?>()
        with(board) {
            for (i in 0 until width) {
                for (j in 0 until width) {
                    boardValues.add(getCellValue(i + 1, j + 1))
                }
            }
        }
        return boardValues == SOLUTION
    }

    override fun processMove(direction: Direction) {
        with(board) {
            val emptyCell: Cell? = find { it == null }
            val neighbourCell = emptyCell?.getNeighbour(direction.reversed()) ?: return
            set(emptyCell, get(neighbourCell))
            set(neighbourCell, null)
        }
    }

    override fun get(i: Int, j: Int): Int? = board.getCellValue(i, j)

    private fun <T> GameBoard<T>.getCellValue(i: Int, j: Int): T? = get(getCell(i, j))
    private fun <T> GameBoard<T>.setCellValue(i: Int, j: Int, value: T) = set(getCell(i, j), value)

}

